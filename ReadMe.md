# Overview

Cup supply service for the beverage project.

```
export VERSION_TAG=`date  +%Y%m%d-dev`
./gradlew build
docker build -t cup-supply:${VERSION_TAG} --build-arg VERSION_TAG=${VERSION_TAG} .
docker run -t cup-supply:${VERSION_TAG}
docker run --read-only --mount type=tmpfs,destination=/tmp -t cup-supply:${VERSION_TAG}
```

