FROM alpine:latest

# export VERSION_TAG=`date  +%Y%m%d-dev`
# ./gradlew build
# docker build -t cup-supply:${VERSION_TAG} --build-arg VERSION_TAG=${VERSION_TAG} .
# docker run -t cup-supply:${VERSION_TAG}
# docker run --read-only -t cup-supply:${VERSION_TAG}
# docker run --read-only --tmpfs /tmp:rw,noexec,nosuid,size=65m -t cup-supply:${VERSION_TAG}

ARG VERSION_TAG

RUN apk update

run apk --no-cache add openjdk8-jre

RUN adduser -D appuser
WORKDIR /home/appuser
USER appuser

ADD ./build/libs/cup-supply-${VERSION_TAG}.jar app.jar

CMD java -jar ${ADDITIONAL_OPTS} app.jar

RUN mkdir /tmp/tomcat static

EXPOSE 9001
